# Horse Track Demo

## Requirements

Horse Track Demo requires Java 8 and Maven 3.


## Build and Run

To build the project, run:

`mvn3 clean package`

The resulting application can be run with maven:

`mvn3 exec:java`

Or manually by executing from the project root directory:

`$ java -jar target/atm-1.0-SNAPSHOT.jar`

