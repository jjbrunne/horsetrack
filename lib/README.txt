This directory contains library dependencies.

The Maven Way to be to allow maven to manage your dependencies.

If there is an offline build requirement, you can uncomment the 'maven-dependency-plugin' section of the project pom.xml.
