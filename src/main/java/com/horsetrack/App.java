package com.horsetrack;


import com.horsetrack.atm.AutomatedTellerMachine;
import com.horsetrack.atm.AutomatedTellerMachineImpl;
import com.horsetrack.dataload.DataLoader;
import com.horsetrack.stable.Stable;
import com.horsetrack.stable.StableImpl;


/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) {

        Stable stable = new StableImpl();
        AutomatedTellerMachine atm = new AutomatedTellerMachineImpl();

        // Initialize state
        DataLoader.loadHorses("horses.csv", stable);
        DataLoader.loadInventory("inventory.csv", atm);
        stable.setWinner(1);

        // Start the main event loop
        CommandManager cm = new CommandManager(System.in, System.out, atm, stable);
        cm.loop();

    }

}
