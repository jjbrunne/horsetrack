package com.horsetrack;

import com.horsetrack.atm.AutomatedTellerMachine;
import com.horsetrack.stable.Horse;
import com.horsetrack.stable.Stable;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 * Created by Joe Brunner on 3/25/17.
 */
public class CommandManager {

    InputStream in;
    PrintStream out;
    AutomatedTellerMachine atm;
    Stable stable;

    public CommandManager(InputStream in, PrintStream out, AutomatedTellerMachine atm, Stable stable) {
        this.in = in;
        this.out = out;
        this.atm = atm;
        this.stable = stable;

    }

    public void loop() {
        printHeader();
        Scanner scanner = new Scanner(in);
        while (true) {
            if (! scanner.hasNextLine()) {
                return;
            }
            String input = scanner.nextLine();
            String[] tokens = input.split("\\s+");

            switch(tokens[0]) {
                case "r":
                case "R":
                    restock();
                    break;

                case "q":
                case "Q":
                    return;

                case "w":
                case "W":
                    if (tokens.length != 2) {
                        invalidCommand(input);
                        break;
                    }

                    try {
                        Integer winnerNumber = Integer.parseInt(tokens[1]);
                        setWinner(winnerNumber);
                    } catch (NumberFormatException e) {
                        invalidCommand(input);
                    }
                    break;

                default:
                    if (tokens.length == 2) {
                        Integer horseNumber = null;
                        Integer betAmount = null;
                        try {
                            horseNumber = Integer.parseInt(tokens[0]);
                        } catch (NumberFormatException e) {
                            out.println("BAD FIRST TOKEN");
                            invalidCommand(input);
                            break;
                        }
                        try {
                            betAmount = Integer.parseInt(tokens[1]);
                        } catch (NumberFormatException e) {
                            invalidBet(tokens[1]);
                            break;
                        }
                        placeBet(horseNumber, betAmount);
                    } else {
                        invalidCommand(input);
                    }
                    break;
            }

            printHeader();
        }
    }

    private void printHeader() {
        printInventory();
        printStable();

    }

    private void restock() {
        atm.restockInventory();
    }

    private void setWinner(int winnerNumber) {
        try {
            stable.setWinner(winnerNumber);
        } catch (Exception e) {
            out.println("Invalid horse: " + winnerNumber);
        }

    }

    private void invalidBet(String betAmount) {
        out.println("Invalid Bet: " + betAmount);
    }

    private void placeBet(Integer horseNumber, Integer betAmount) {
        Horse horse = stable.getHorse(horseNumber);
        if (horse == null) {
            invalidHorse(horseNumber);
            return;
        }
        if (stable.getHorse(horseNumber).equals(stable.getWinner())) {
            winningBet(horseNumber, betAmount);
        } else {
            losingBet(horseNumber);
        }
    }

    private void invalidCommand(String command) {
        out.println("Invalid command: " + command);
    }

    private void invalidHorse(int horseNumber) {
        out.println("Invalid Horse Number: " + horseNumber);
    }

    private void winningBet(int horseNumber, int betAmount) {
        Horse horse = stable.getHorse(horseNumber);
        int payout = horse.getOdds() * betAmount;
        if (! atm.canPayOut(payout)) {
            out.println("Insufficient Funds: " + payout);
            return;
        }
        out.println("Payout: " + horse.getName() + ",$" + payout);
        out.println("Dispensing:");
        List<Map.Entry<Integer, Integer>> payoutDetails = atm.payOut(payout).entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toList());

        payoutDetails.forEach(entry -> {
            out.println("$" + entry.getKey() + "," + entry.getValue());
        });
    }

    private void losingBet(int horseNumber) {
        out.println("No Payout: " + stable.getHorse(horseNumber).getName());
    }

    private void printStable() {
        out.println("Horses:");
        Horse winner = stable.getWinner();
        Map<Integer, Horse> horses = stable.getHorses();
        List<Map.Entry<Integer, Horse>> horseList = horses.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toList());

        horseList.forEach( entry -> {
            Integer horseNumber = entry.getKey();
            Horse horse = entry.getValue();
            String isWinner = (winner != null && winner.equals(horse)) ? "won" : "lost";
            out.println(horseNumber
                    + "," + horse.getName()
                    + "," + horse.getOdds()
                    + "," + isWinner);
        });

    }

    private void printInventory() {
        out.println("Inventory:");

        Map<Integer, Integer> inventory = atm.getInventory();
        List<Map.Entry<Integer, Integer>> denominationList = inventory.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toList());

        denominationList.forEach( entry -> {
            out.println("$" + entry.getKey()
                    + ","
                    +  entry.getValue());
        });
    }

}
