package com.horsetrack.atm;

import java.util.Map;

/**
 * Created by Joe Brunner on 3/23/17.
 */
public interface AutomatedTellerMachine {

    /**
     * Sets the inventory to contain the values provided
     * @param limits Mapping of denomination of bills to the number of those bills to set in inventory
     */
    public void setInventoryLimits(Map<Integer, Integer> limits) throws RuntimeException;

    public Map<Integer, Integer> getInventory();

    /**
     * Restocks the inventory to the predefined maximum values
     */
    public void restockInventory();

    /**
     * Determines whether the AutomatedTellerMachine can manage the provided payout value given the current inventory
     * @param payOut The amount of cash that we are considering for payout.
     * @return boolean indicating whether the payout value under consideration can be handled by the current inventory
     */
    public boolean canPayOut(int payOut);

    /**
     * Pays out the current requested value, decreasing inventory appropriately
     * @param payOut The amount of cash that we are paying out.
     * @return Map describing the number of each denomination bill that makes up the payout.
     */
    public Map<Integer, Integer> payOut(int payOut);

}
