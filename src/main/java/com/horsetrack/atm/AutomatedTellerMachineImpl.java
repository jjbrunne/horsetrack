package com.horsetrack.atm;


import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * ATM - an Automated Teller Machine for the Horse Track Park
 *
 */

public class AutomatedTellerMachineImpl implements AutomatedTellerMachine {

    /* The current inventory */
    private Map<Integer, Integer> inventory = new HashMap<>();

    /* The limits to apply when restocking */
    private Map<Integer, Integer> inventoryLimits = new HashMap<>();


    @Override
    public void setInventoryLimits(Map<Integer, Integer> limits) throws RuntimeException {
        if (limits == null) {
            throw new RuntimeException("Invalid parameter.  Limits must not be null");
        }
        inventoryLimits.clear();
        inventoryLimits.putAll(limits);
    }

    protected Map<Integer, Integer> getInventoryLimits() {
        return Collections.unmodifiableMap(inventoryLimits);
    }

    @Override
    public Map<Integer, Integer> getInventory() {
        return Collections.unmodifiableMap(inventory);
    }

    @Override
    public void restockInventory() {
        inventory.clear();
        inventory.putAll(inventoryLimits);
    }

    @Override
    public boolean canPayOut(int payOut) {
        return calculateTotalInventoryValue() >= payOut;
    }

    @Override
    public Map<Integer, Integer> payOut(int payOut) {

        // If the total value of the inventory is less than the requested payout, we cannot payout.
        if (! canPayOut(payOut)) {
            throw new RuntimeException("Invalid payout.  Insufficient funds");
        }

        // Calculate the count of each bill in the payout and reduce the inventory
        Map<Integer, Integer> payOutValues = calculatePayOutValues(payOut);
        for (Map.Entry<Integer, Integer> payOutValue : payOutValues.entrySet()) {
            inventory.put(payOutValue.getKey(), inventory.get(payOutValue.getKey()) - payOutValue.getValue());
        }
        return payOutValues;
    }

    /**
     * Determine the total current worth of this ATM
     * @return total value
     */
    protected int calculateTotalInventoryValue() {
        int totalInventoryValue = 0;
        for (Map.Entry<Integer, Integer> entry : inventory.entrySet()) {
            totalInventoryValue += entry.getKey() * entry.getValue();
        }
        return totalInventoryValue;
    }

    /**
     * Compute how many of each bill will make up the requested payout given the current inventory
     * @param payOut the requested payout value
     * @return A description of how many of each denomination
     */
    private Map<Integer, Integer> calculatePayOutValues(int payOut) {

        // initialize each denomination in the payout to 0
        Map<Integer, Integer> payOutValues = new HashMap<>();
        inventoryLimits.keySet().stream().forEach(denomination -> {
            payOutValues.put(denomination, 0);
        });

        // get a sorted list of inventory denomination count
        List<Map.Entry<Integer, Integer>> sortedInventory = inventory.entrySet().stream()
                .sorted(Map.Entry.<Integer, Integer>comparingByKey().reversed()).collect(Collectors.toList());

        // for each denomination, starting with the largest, calculate the maximum amount that we can
        //   reduce total remaining payOut using the current inventory of this denomination
        for (Map.Entry<Integer, Integer> entry : sortedInventory) {

            int billCount = payOut / entry.getKey();

            // limit the count of this denomination in the payout according to current inventory for that denomination
            if (billCount > entry.getValue()) {
                billCount = entry.getValue();
            }

            // store the number of bills for this denomination in the payout results
            payOutValues.put(entry.getKey(), billCount);

            // reduce the remaining payout value by the amount payed out in this denomination
            payOut = payOut - (entry.getKey() * billCount);


            // stop iterating if we've completed our payout
            if (payOut == 0) {
                break;
            }
        }

        return payOutValues;
    }

}
