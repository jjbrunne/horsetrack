package com.horsetrack.dataload;

import com.horsetrack.atm.AutomatedTellerMachine;
import com.horsetrack.stable.Horse;
import com.horsetrack.stable.Stable;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Utility class to load horses into the Stable and money into the ATM
 */
public class DataLoader {

    public static void loadHorses(String horsesFileName, Stable stable) {
        if (horsesFileName == null) {
            throw new RuntimeException("Unable to load horses.  Data file is empty");
        }
        List<String> lines = getFileLines(horsesFileName);
        int i = 1;
        for (String line : lines) {
            String[] horseAndOdds = line.split(",");
            stable.setHorse(i, new Horse(horseAndOdds[0], Integer.valueOf(horseAndOdds[1])));
            i++;
        }
    }

    public static void loadInventory(String inventoryFileName, AutomatedTellerMachine atm) {
        if (inventoryFileName == null) {
            throw new RuntimeException("Unable to load inventory.  Data file is empty");
        }

        Map<Integer, Integer> inventoryLimits = new HashMap<>();

        List<String> lines = getFileLines(inventoryFileName);
        for (String line : lines) {
            String[] denominationAndLimit = line.split(",");
            inventoryLimits.put(Integer.valueOf(denominationAndLimit[0]), Integer.valueOf(denominationAndLimit[1]));
        }
        atm.setInventoryLimits(inventoryLimits);
        atm.restockInventory();;
    }

    private static List<String> getFileLines(String fileName) {
        if (fileName == null) {
            throw new RuntimeException("Unable to load data file.  Filename was empty");
        }
        try {
            return Files.readAllLines(Paths.get(fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
