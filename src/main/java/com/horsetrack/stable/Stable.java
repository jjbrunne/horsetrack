package com.horsetrack.stable;

import java.util.List;
import java.util.Map;

/**
 * A Stable represents a collection of horses and the latest winning horse.
 */
public interface Stable {


    void setHorses(List<Horse> horses);
    Map<Integer, Horse> setHorse(int horseNumber, Horse horse);
    Map<Integer, Horse> getHorses();
    Horse getHorse(int horseNumber);
    Map<Integer, Horse> setWinner(int winningNumber);
    Horse getWinner();

}
