package com.horsetrack.stable;

import java.util.*;

/**
 * Stable reference implementation
 */
public class StableImpl implements Stable {

    Map<Integer, Horse> horses = new HashMap<>();
    Horse winner = null;

    @Override
    public void setHorses(List<Horse> horses) {
        this.horses.clear();
        for (int i = 0; i < horses.size(); i++) {
            setHorse(i+1, horses.get(i));
        }
    }

    @Override
    public Map<Integer, Horse> setHorse(int horseNumber, Horse horse) {
        horses.put(horseNumber, horse);
        return horses;
    }

    @Override
    public Map<Integer, Horse> getHorses() {
        return horses;
    }

    @Override
    public Horse getHorse(int horseNumber) {
        return horses.get(horseNumber);
    }

    @Override
    public Map<Integer, Horse> setWinner(int winnerNumber) {
        Horse winningHorse = getHorse(winnerNumber);
        if (winningHorse == null) {
            throw new RuntimeException("Unknown winning horse");
        }
        winner = winningHorse;
        return horses;
    }

    @Override
    public Horse getWinner() {
        return winner;
    }
}
