package com.horsetrack;

import com.horsetrack.atm.AutomatedTellerMachine;
import com.horsetrack.atm.AutomatedTellerMachineImpl;
import com.horsetrack.dataload.DataLoader;
import com.horsetrack.stable.Stable;
import com.horsetrack.stable.StableImpl;
import org.junit.Test;


import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.*;

/**
 * Created by Joe Brunner on 3/26/17.
 *
 * Each test is run against a sample input file and compares the output to an expected output file
 *
 */
public class CommandManagerTest {

    private CommandManager cm;

    InputStream in;

    PrintStream out;

    ByteArrayOutputStream outStream;

    /**
     * Run the sample test from the instructions
     * @throws IOException
     */
    @Test
    public void testSampleRun() throws IOException {
        setupTest("test1.input");
        cm.loop();
        verifyOutput("test1.output");
    }

    private void verifyOutput(String expectedOutputFile) throws IOException {
        String expectedOutput = new String(Files.readAllBytes(Paths.get(expectedOutputFile)));
        assertEquals(expectedOutput, outStream.toString());
    }


    private void setupTest(String inputFile) throws FileNotFoundException {
        // Sample Input
        in = new FileInputStream(new File(inputFile));

        // Verifiable Output
        outStream = new ByteArrayOutputStream();
        out = new PrintStream(outStream, true);

        AutomatedTellerMachine atm = new AutomatedTellerMachineImpl();
        Stable stable = new StableImpl();

        // Thew CommandManager
        cm = new CommandManager(in, out, atm, stable);

        // Run the default data load
        DataLoader.loadHorses("horses.csv", stable);
        DataLoader.loadInventory("inventory.csv", atm);
        stable.setWinner(1);
    }

}
