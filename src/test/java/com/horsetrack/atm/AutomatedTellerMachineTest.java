package com.horsetrack.atm;

import org.hamcrest.CoreMatchers;
import org.junit.*;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


public class AutomatedTellerMachineTest {


    private AutomatedTellerMachineImpl atm;

    private static Map<Integer, Integer> defaultMaximums;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @BeforeClass
    public static void beforeClass() {
        Map<Integer, Integer> maximums = new HashMap<>();
        maximums.put(1, 5);
        maximums.put(2, 5);
        maximums.put(5, 5);
        maximums.put(10, 5);
        maximums.put(20, 5);
        maximums.put(100, 5);
        defaultMaximums = Collections.unmodifiableMap(maximums);
    }

    @Before
    public void beforeTest() {
        atm = new AutomatedTellerMachineImpl();
        atm.setInventoryLimits(defaultMaximums);
        atm.restockInventory();
    }

    @Test
    public void testUninitializedAutomatedTellerMachine() {
        AutomatedTellerMachineImpl newAtm = new AutomatedTellerMachineImpl();
        assert(newAtm.getInventory().isEmpty());
    }

    @Test
    public void testSetInventoryLimits() {
        AutomatedTellerMachineImpl newAtm = new AutomatedTellerMachineImpl();
        newAtm.setInventoryLimits(defaultMaximums);
        assertEquals("Inventory limits does not match expectation", defaultMaximums, newAtm.getInventoryLimits());
    }

    @Test
    public void testSetInventoryLimitsClearsOldLimits() {

        // this should clear out all of the previous limits, and only retain a value of 1
        Map<Integer, Integer> newMaximums = new HashMap<>();
        newMaximums.put(1, 10);
        atm.setInventoryLimits(newMaximums);

        // the new limits should not contain values for a denomination of 5
        assertFalse("Previous inventory limits were not cleared out when setting new inventory limits.", atm.getInventoryLimits().containsKey(5));
    }


    @Test
    public void testSetInventoryLimitsToNullThrowsException() {
        thrown.expect(RuntimeException.class);
        thrown.expectMessage(CoreMatchers.startsWith("Invalid parameter"));

        AutomatedTellerMachineImpl newAtm = new AutomatedTellerMachineImpl();
        newAtm.setInventoryLimits(null);
        assertEquals("Inventory limits does not match expectation", defaultMaximums, newAtm.getInventoryLimits());
    }

    @Test
    public void testRestockedInventoryMatchesCurrentLimits() {

        Map<Integer, Integer> newMaximums = new HashMap<>();
        newMaximums.put(1, 10);

        atm.setInventoryLimits(newMaximums);
        atm.restockInventory();
        assertEquals("Restocked inventory does not match limits", newMaximums, atm.getInventory());
    }

    @Test
    public void testRestockInventoryRemovesUnsupportedDenominations() {
        assertTrue(atm.getInventory().containsKey(5));
        Map<Integer, Integer> newMaximums = new HashMap<>();
        newMaximums.put(1, 10);
        atm.setInventoryLimits(newMaximums);
        atm.restockInventory();

        // restocked inventory should no longer contain $5 denomination
        assertFalse("Inventory still contains previous denomination after restock", atm.getInventory().containsKey(5));
    }

    @Test
    public void testCanPayOutRejectsValuesAboveLimit() {
        int totalValue = atm.calculateTotalInventoryValue();
        assertFalse(atm.canPayOut(totalValue + 1));
    }

    @Test
    public void testCanPayOutAcceptsValueBelowLimit() {
        int totalValue = atm.calculateTotalInventoryValue();
        assertTrue(atm.canPayOut(totalValue - 1));
    }

    @Test
    public void testPayOutRejectsValueAboveLimit() {
        thrown.expect(RuntimeException.class);
        thrown.expectMessage(CoreMatchers.startsWith("Invalid payout"));

        int totalValue = atm.calculateTotalInventoryValue();
        atm.payOut(totalValue + 1);
    }

    @Test
    public void testPayOutCalculationForFullInventory() {
        int totalValue = atm.calculateTotalInventoryValue();
        Map<Integer, Integer> payOut = atm.payOut(totalValue);
        assertEquals(payOut, defaultMaximums);
    }

    @Test
    public void testPayOutCalculationForSmallerAmount() {
        int totalValue = 10;
        Map<Integer, Integer> payOut = atm.payOut(totalValue);
        Map<Integer, Integer> correctPayOut = new HashMap<>();
        defaultMaximums.keySet().stream().forEach(denomination -> {
            correctPayOut.put(denomination, 0);
        });
        correctPayOut.put(10, 1);
        assertEquals(payOut, correctPayOut);
    }

    @Test
    public void testPayOutDoesNotExceedInventoryLimitOfLargestDenomination() {
        int payOutValue = 80;

        // cash out all but 1 of the $20s
        Map<Integer, Integer> payOut = atm.payOut(payOutValue);
        Map<Integer, Integer> correctPayOut = new HashMap<>();
        defaultMaximums.keySet().stream().forEach(denomination -> {
            correctPayOut.put(denomination, 0);
        });

        correctPayOut.put(20, 4);
        assertEquals(correctPayOut, payOut);

        // This payout could be satisfied with 2 $20, but we only have 1
        payOutValue = 46;
        payOut = atm.payOut(payOutValue);

        // Validate that only 1 $20 was used in the payout.
        correctPayOut.clear();
        defaultMaximums.keySet().stream().forEach(denomination -> {
            correctPayOut.put(denomination, 0);
        });
        correctPayOut.put(20, 1);
        correctPayOut.put(10, 2);
        correctPayOut.put(5, 1);
        correctPayOut.put(1, 1);
        assertEquals(correctPayOut, payOut);

    }

    @Test
    public void testRestockAfterPayout() {
        int initialValue = atm.calculateTotalInventoryValue();
        atm.payOut(initialValue - 13);
        int valueAfterPayout = atm.calculateTotalInventoryValue();
        assertEquals(13, valueAfterPayout);
        atm.restockInventory();
        assertEquals(initialValue, atm.calculateTotalInventoryValue());
    }


}
