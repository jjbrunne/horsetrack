package com.horsetrack.stable;

import com.horsetrack.atm.AutomatedTellerMachineImpl;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.*;

import static org.junit.Assert.*;


public class StableTest {


    private StableImpl stable;

    private static List<Horse> horses;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @BeforeClass
    public static void beforeClass() {
        List<Horse> newHorses = new ArrayList<>();
        newHorses.add(new Horse("Joe", 3));
        newHorses.add(new Horse("John", 5));
        newHorses.add(new Horse("Jim", 7));
        horses = Collections.unmodifiableList(newHorses);
    }

    @Before
    public void beforeTest() {
        stable = new StableImpl();
        stable.setHorses(horses);
    }

    @Test
    public void testUninitializedStable() {
        StableImpl newStable = new StableImpl();
        assertTrue("Uninitialized stable is not empty", newStable.getHorses().isEmpty());
        assertEquals(null, newStable.getWinner());
    }

    @Test
    public void testSetHorses() {
        StableImpl newStable = new StableImpl();
        newStable.setHorses(horses);
        List<Horse> newHorseSet = new ArrayList<>(newStable.getHorses().values());
        assertEquals("Inventory limits does not match expectation", horses, newHorseSet);
    }

    @Test
    public void testSetHorsesRemovesOldHorses() {
        List<Horse> newHorses = new ArrayList<>();
        newHorses.add(new Horse("Todd", 1));
        newHorses.add(new Horse("Bill", 2));
        stable.setHorses(newHorses);
        assertNull(stable.getHorse(3));
    }

    @Test
    public void testSetHorseWithNewHorse() {
        Horse karen = new Horse("Karen", 5);
        stable.setHorse(4, karen);
        assertEquals(karen, stable.getHorse(4));
    }

    @Test
    public void testSetHorseOverwritesExistingHorse() {
        Horse oldJoe = stable.getHorse(1);
        assertNotNull(oldJoe);
        assertEquals(3, oldJoe.getOdds());
        Horse newJoe = new Horse("Joe", 10);
        stable.setHorse(1, newJoe);
        assertEquals(10, stable.getHorse(1).getOdds());
    }


}
